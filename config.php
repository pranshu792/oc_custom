<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/opencar/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/opencar/');

// DIR
define('DIR_APPLICATION', 'C:\wamp\www\opencar/catalog/');
define('DIR_SYSTEM', 'C:\wamp\www\opencar/system/');
define('DIR_DATABASE', 'C:\wamp\www\opencar/system/database/');
define('DIR_LANGUAGE', 'C:\wamp\www\opencar/catalog/language/');
define('DIR_TEMPLATE', 'C:\wamp\www\opencar/catalog/view/theme/');
define('DIR_CONFIG', 'C:\wamp\www\opencar/system/config/');
define('DIR_IMAGE', 'C:\wamp\www\opencar/image/');
define('DIR_CACHE', 'C:\wamp\www\opencar/system/cache/');
define('DIR_DOWNLOAD', 'C:\wamp\www\opencar/download/');
define('DIR_LOGS', 'C:\wamp\www\opencar/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'abc123');
define('DB_DATABASE', 'opencar');
define('DB_PREFIX', 'oc_');
?>